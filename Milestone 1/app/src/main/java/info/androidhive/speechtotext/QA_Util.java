package info.androidhive.speechtotext;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Cojocaru on 14/04/2016.
 */
public class QA_Util
{
    private HashMap hm = new HashMap();

    public  QA_Util(InputStream input) {
        String str, question, line, faceType;
        ArrayList<String> lineSplitted;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            if (input != null)
            {
                try
                {
                    readInput(reader);
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        } finally {
            try {
                input.close();
            } catch (Throwable ignore) {
            }
        }
    }

    public void readInput(BufferedReader reader) throws IOException {
        String str, question, line, faceType;
        ArrayList<String> lineSplitted;
        while ((str = reader.readLine()) != null)
        {
            line = str;
            lineSplitted =  new ArrayList<String>(Arrays.asList(line.split(";")));
            question = lineSplitted.remove(0);
            faceType = lineSplitted.remove(lineSplitted.size() - 1);
            hm.put(question, new AnswerStruct(lineSplitted, faceType));
        }
    }

    public  AnswerStruct searchQuestion(String str)
    {
        Set set = hm.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext())
        {
            Map.Entry me = (Map.Entry) i.next();
            if (str.contains((CharSequence) me.getKey()))
            {
                return (AnswerStruct) me.getValue();
            }
        }
        return null;
    }
}

class AnswerStruct
{
    private ArrayList<String> answers ;
    private String faceType;

    public AnswerStruct( ArrayList<String> answers, String faceType)
    {
        this.answers = (ArrayList<String>) answers.clone();
        this.faceType = faceType;
    }

    public String getRandAnswer()
    {
        int index = (int) Math.floor( Math.random() * this.answers.size() ) ;
        return this.answers.get(index);
    }

    public String  getFaceType() {return this.faceType;}
}