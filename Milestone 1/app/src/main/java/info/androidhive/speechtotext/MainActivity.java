package info.androidhive.speechtotext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends Activity {

	private TextView txtSpeechInput;
	private TextToSpeech tts;
	private  QA_Util qa_Util ;
	private final int REQ_CODE_SPEECH_INPUT = 100;
	private boolean REMEMBER_OR_SEARCH = false;
	private String question;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initializeComponents();
		// hide the action bar
		getActionBar().hide();


		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
	}

	/**
	 * Showing google speech input dialog
	 */
	public void promptSpeechInput(View view) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));

		try {
			startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
		} catch (ActivityNotFoundException a) {
			Toast.makeText(getApplicationContext(),
					getString(R.string.speech_not_supported),
					Toast.LENGTH_SHORT).show();
		}
	}

	public void talkBack() {
		if (REMEMBER_OR_SEARCH == true)
		{
			REMEMBER_OR_SEARCH = false;
			rememberOrSearch(txtSpeechInput.getText().toString());
			return;
		}
		String question = txtSpeechInput.getText().toString();
		AnswerStruct answerStruct = qa_Util.searchQuestion(question);
		Toast.makeText(getApplicationContext(), question, Toast.LENGTH_SHORT).show();
		if (answerStruct != null)
		{
			changeFace(answerStruct.getFaceType());
			tts.speak(answerStruct.getRandAnswer(), TextToSpeech.QUEUE_FLUSH, null);
		}
		else
		{
			String dontKnow = "I don't know the answer. Would you like" +
					"to remember an answer, or should I search the web?";
			tts.speak(dontKnow, TextToSpeech.QUEUE_FLUSH, null);
			REMEMBER_OR_SEARCH = true;
			SystemClock.sleep(6000);
			this.question = question;
			promptSpeechInput(findViewById(android.R.id.content));
		}
	}

	public void searchWeb(View view) {
		String toSearch = txtSpeechInput.getText().toString();
		searchWeb(toSearch);
	}

	public void searchWeb(String toSearch) {
		Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
		intent.putExtra(SearchManager.QUERY, toSearch);
		startActivity(intent);
	}

	public void rememberOrSearch(String answer)
	{
		answer = answer.toLowerCase();
		if (answer.contains("search"))
			searchWeb(question);
		else
			rememberAnswer();
	}
	public void rememberAnswer()
	{
	}

	public void changeFace(String face)
	{
		ImageButton button;
		button= (ImageButton)findViewById(R.id.imageButton);
		if(face.equals("EVIL"))
			button.setBackgroundResource(R.drawable.ic_evil);
		else if(face.equals("GRUMPY"))
			button.setBackgroundResource(R.drawable.ic_grumpy);
		else if(face.equals("HAPPY"))
			button.setBackgroundResource(R.drawable.ic_happy);
		else if(face.equals("PLAYFUL"))
			button.setBackgroundResource(R.drawable.ic_playful);
		else
			button.setBackgroundResource(R.drawable.ic_sad);
	}
	/**
	 * Receiving speech input
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
			case REQ_CODE_SPEECH_INPUT: {
				if (resultCode == RESULT_OK && null != data) {
					ArrayList<String> result = data
							.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
					txtSpeechInput.setText(result.get(0));
					talkBack();
				}
				break;
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void initializeComponents() {
		txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);

		tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				if (status != TextToSpeech.ERROR) {
					tts.setLanguage(Locale.UK);
				}
			}
		});
		try {
			AssetManager am = this.getAssets();
			InputStream is = am.open("QA");
			qa_Util = new QA_Util(is);

		} catch (IOException e) {
			Log.v("a", "nu mere");
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"Main Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this auto-generated app deep link URI is correct.
				Uri.parse("android-app://info.androidhive.speechtotext/http/host/path")
		);
		AppIndex.AppIndexApi.start(client, viewAction);
	}

	@Override
	public void onStop() {
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		Action viewAction = Action.newAction(
				Action.TYPE_VIEW, // TODO: choose an action type.
				"Main Page", // TODO: Define a title for the content shown.
				// TODO: If you have web page content that matches this app activity's content,
				// make sure this auto-generated web page URL is correct.
				// Otherwise, set the URL to null.
				Uri.parse("http://host/path"),
				// TODO: Make sure this auto-generated app deep link URI is correct.
				Uri.parse("android-app://info.androidhive.speechtotext/http/host/path")
		);
		AppIndex.AppIndexApi.end(client, viewAction);
		client.disconnect();
	}
}
