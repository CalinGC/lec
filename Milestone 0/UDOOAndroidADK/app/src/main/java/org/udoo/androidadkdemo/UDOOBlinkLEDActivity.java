package org.udoo.androidadkdemo;

import me.palazzetti.adktoolkit.AdkManager;
import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

public class UDOOBlinkLEDActivity extends Activity{
	
//	private static final String TAG = "UDOO_AndroidADK";

	private AdkManager mAdkManager;
	
	private ToggleButton buttonLED;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		mAdkManager = new AdkManager((UsbManager) getSystemService(Context.USB_SERVICE));
		
//		register a BroadcastReceiver to catch UsbManager.ACTION_USB_ACCESSORY_DETACHED action
		registerReceiver(mAdkManager.getUsbReceiver(), mAdkManager.getDetachedFilter());
		
		buttonLED = (ToggleButton) findViewById(R.id.toggleButtonLED);		
		
		Log.i("ADK manager", "available: " + mAdkManager.serialAvailable());
	}
 
	@Override
	public void onResume() {
		super.onResume(); 
		mAdkManager.open();
	}
 
	@Override
	public void onPause() {
		super.onPause();
		mAdkManager.close();
	}
	
	@Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mAdkManager.getUsbReceiver());
    }
 
	// ToggleButton method - send message to the SAM3X with the AdkManager
	public void blinkLED(View v) {
		if (buttonLED.isChecked()) { 
			// writeSerial() allows you to write a single char or a String object.
			mAdkManager.writeSerial("1");
		} else {
			mAdkManager.writeSerial("0"); 
		}	
	}
 
}
